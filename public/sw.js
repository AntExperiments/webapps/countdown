// Update cache names any time any of the cached files change.
const CACHE_NAME = 'gay-v2';

// Add list of files to cache here.
const FILES_TO_CACHE = [
	'./offline.html',
	'./',
	'./index.html',
	'./images/192x192.png',
	'./images/144x144.png',
	'./images/72x72.png',
	'./images/96x96.png',
	'./images/48x48.png',
	'./global.css',
	'./manifest.json',
	'./build/bundle.css',
	'./build/bundle.js.map',
	'./build/bundle.js',
];

self.addEventListener('install', (evt) => {
  console.log('[ServiceWorker] Install');

  evt.waitUntil(
      caches.open(CACHE_NAME).then((cache) => {
        console.log('[ServiceWorker] Pre-caching offline page');
        return cache.addAll(FILES_TO_CACHE);
      })
  );

  self.skipWaiting();
});

self.addEventListener('activate', (evt) => {
  console.log('[ServiceWorker] Activate');
  // Remove previous cached data from disk.
  evt.waitUntil(
      caches.keys().then((keyList) => {
        return Promise.all(keyList.map((key) => {
          if (key !== CACHE_NAME) {
            console.log('[ServiceWorker] Removing old cache', key);
            return caches.delete(key);
          }
        }));
      })
  );

  self.clients.claim();
});

self.addEventListener('fetch', e => {
  console.log('[ServiceWorker] Fetch', e.request.url);
  e.respondWith(
    caches.match(e.request).then(response => response || fetch(e.request)).catch(() => caches.match('/offline.html'))
  );
})